# K8s over container

## Run container


~~~
docker run -it --name ubuntuk8s -v /var/run/docker.sock:/var/run/docker.sock ubuntu:latest
~~~

## RECIPE:

* `apt-update && apt-get -y install wget curl build-essential git vim`
* `apt-get update && apt-get -y install wget curl build-essential git vim`
* `apt-get install -y apt-transport-https ca-certificates gnupg-agent software-properties-common`
* `curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -`
* `apt-key fingerprint 0EBFCD88`
* `add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"`
* `apt-get update && apt-get -y install docker-ce-cli`
* `docker run hello-world`
* `curl -Lo ./kubectl https://storage.googleapis.com/kubernetes-release/release/v1.17.0/bin/linux/amd64/kubectl`
* `chmod +x ./kubectl`
* `mv ./kubectl /usr/local/bin/kubectl`
* `curl -Lo ./kind "https://github.com/kubernetes-sigs/kind/releases/download/v0.7.0/kind-$(uname)-amd64"`
* `chmod +x ./kind`
* `mv ./kind /usr/local/bin/kind`

# K8s labs and practice
@jorgemustaine 2020
![k8s](static/images/k8s_ico.jpg)
Use of the set of tools to k8s deploy and administration.

## related techs:

* [Vagrant](https://www.vagrantup.com/)
* [Ansible](https://www.ansible.com/)
* [k8s](https://kubernetes.io/es/)
* [k3s](https://k3s.io/)
* [kind](https://kind.sigs.k8s.io/)

